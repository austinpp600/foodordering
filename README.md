**how to build, install and use the project.**

1. Create a new directoryfor your webapplication(for example: food)and open this directory inVSCOD
2. Use "npm init" in the terminal of the project directory to initiate the npm
3. Use "npm install" to install express and ejs packagesusing your terminal
4. Confirm that the dependencies have been installed by inspecting your package.json file
5. Restart your server by typing "ctrl+c"
6. type "npm index.js" to run the application

**MIT License**

## Copyright (c) 2020 austin d cheeran

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

**Reason to Choose this license**
MIT license is a very simple and short license. The only condition in MIT license is the preservation of copyright
and license notices. Hence in MIT license, larger works will be distributedinto different terms and also without the
source code. Hence i find it ideal license for my project